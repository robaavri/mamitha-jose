//
//  Branch.swift
//  BranchListings
//
//  Created by Mamitha Jose on 3/11/19.
//  Copyright © 2019 Mamitha Jose. All rights reserved.
//

import Foundation
import CoreData
import CoreLocation

class Branch: NSManagedObject {

    var location: CLLocation? {
        get {
            return CLLocation(latitude: latitude, longitude: longitude)
        }
    }
    
    var completeAddress: String? {
        get {
            var address = ""
            if let addressLine = self.addressLine {
                address += addressLine
                if let city = city {
                    address += "\n\(city)"
                }
                
                if let county = countrySubDivision {
                    address += "\n\(county)"
                    if let country = country {
                        address += ", \(country)"
                    }
                    if let postCode = postCode {
                        address += " - \(postCode)"
                    }
                }
                else if let country = country {
                    address += "\n\(country)"
                    if let postCode = postCode {
                        address += " - \(postCode)"
                    }
                }
                else {
                    if let postCode = postCode {
                        address += "\n\(postCode)"
                    }
                }
            }
            return address
        }
    }
}
