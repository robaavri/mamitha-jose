//
//  CoreDataController.swift
//  BranchListings
//
//  Created by Mamitha Jose on 3/11/19.
//  Copyright © 2019 Mamitha Jose. All rights reserved.
//

import Foundation
import CoreData

class CoreDataController {
    
    static var context: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    static var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "BranchListings")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    class func saveContext () -> Bool {
        if context.hasChanges {
            do {
                try context.save()
                return true
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
        return false
    }
    
    //Clean db
    class func cleanDelete() {
        let branchFetchRequest: NSFetchRequest<Branch> = Branch.fetchRequest()
        if let result = try? context.fetch(branchFetchRequest) {
            for object in result {
                context.delete(object)
            }
        }
        
        let workingHoursFetchRequest: NSFetchRequest<WorkingHours> = WorkingHours.fetchRequest()
        if let result = try? context.fetch(workingHoursFetchRequest) {
            for object in result {
                context.delete(object)
            }
        }
    }
    
    //fetchRequest wrappers
    class func getBranches() -> [Branch]? {
        var branches:[Branch]? = nil
        let branchFetchRequest: NSFetchRequest<Branch> = Branch.fetchRequest()
        do { branches = try context.fetch(branchFetchRequest) }
        catch {}
        return branches
    }
    
    class func getBranchesInCity(_ city:String) -> [Branch]? {
        var branches:[Branch]? = nil
        let branchFetchRequest: NSFetchRequest<Branch> = Branch.fetchRequest()
        
        let predicate = NSPredicate(format: "city CONTAINS[c] %@", city)
        branchFetchRequest.predicate = predicate
        do { branches = try context.fetch(branchFetchRequest) }
        catch {}
        return branches
    }
}
