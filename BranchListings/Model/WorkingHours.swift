//
//  WorkingHours.swift
//  BranchListings
//
//  Created by Mamitha Jose on 3/11/19.
//  Copyright © 2019 Mamitha Jose. All rights reserved.
//

import Foundation
import CoreData

class WorkingHours: NSManagedObject {
    var day : String? {
        get {
            return String(dayString?.dropFirst() ?? "")
        }
    }
}
