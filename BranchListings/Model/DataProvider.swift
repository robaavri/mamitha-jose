//
//  DataProvider.swift
//  BranchListings
//
//  Created by Mamitha Jose on 3/11/19.
//  Copyright © 2019 Mamitha Jose. All rights reserved.
//

import Foundation

typealias BranchRequestCompletion = (_ error:Error?, _ success:Bool) -> Void
private let branchListUrl = "https://api.halifax.co.uk/open-banking/v2.2/branches"

class DataProvider {
    
    static let shared = DataProvider()
    
    public func requestBranches(completion:@escaping BranchRequestCompletion) {
        
        NetworkController.requestData(withMethod: "GET", url: branchListUrl) { (data, statusCode, error) in
            guard statusCode == 200, let data = data else { return }
            do {
                let parsedData = try JSONSerialization.jsonObject(with:data) as! NSDictionary
                let branches = self.branchesWithJSON(json: parsedData)
                if let _ = branches {
                    let saveSuccess = CoreDataController.saveContext()
                    completion(error, saveSuccess)
                }
                else {
                    completion(error, false)
                }
            } catch { }
        }
    }
}

//MARK:- JSON Parsing.
//Used the native approach
extension DataProvider {
    
    private func branchesWithJSON(json: NSDictionary) -> [Branch]? {
        var branches = [Branch]()
        
        guard let dataItems = json["data"] as? NSArray,
            let data = dataItems.firstObject as? NSDictionary,
            let brand = data["Brand"] as? NSArray,
            let brandData = brand.firstObject as? NSDictionary,
            let branchList = brandData["Branch"] as? NSArray
            else {
                return nil
        }
        
        branchList.forEach {
            if let branch = branchWithJSON($0 as! NSDictionary) {
                branches.append(branch)
            }
        }
        return branches
    }
    
    private func branchWithJSON(_ data: NSDictionary) -> Branch? {
        
        let branch = Branch(context: CoreDataController.context)
        
        //id
        let id = data["Identification"] as? String
        assert(id != nil, "Invalid parameter")
        branch.id = id
        
        //name
        if let name = data["Name"] as? String {
            branch.name = name.camelCasedString
        }
        
        //phone and fax
        if let contactInfo = data["ContactInfo"] as? NSArray,
            let phoneContact = contactInfo.firstObject as? NSDictionary,
            phoneContact["ContactType"] as? String == "Phone" {
            
            branch.phoneNumber = phoneContact["ContactContent"] as? String
            
            if let phoneContact = contactInfo.lastObject as? NSDictionary,
                phoneContact["ContactType"] as? String == "Fax" {
                
                branch.faxNumber = phoneContact["ContactContent"] as? String
            }
        }
        
        //address and location
        if let postalAddress = data["PostalAddress"] as? NSDictionary {
            
            if let addressLine  = postalAddress["AddressLine"] as? Array<String> {
                branch.addressLine = addressLine.joined(separator: " ").camelCasedString
            }
            
            //location
            if let geoLocation = postalAddress["GeoLocation"] as? NSDictionary,
                let geographicCoordinates = geoLocation["GeographicCoordinates"] as? NSDictionary,
                let long = geographicCoordinates["Longitude"] as? String,
                let longitude = Double(long),
                let lat = geographicCoordinates["Latitude"] as? String,
                let latitude = Double(lat) {
                branch.longitude = longitude
                branch.latitude = latitude
            }
            
            if let city = postalAddress["TownName"] as? String {
                branch.city = city.camelCasedString
            }
            branch.country = postalAddress["Country"] as? String
            branch.postCode = postalAddress["PostCode"] as? String
            
            if let countrySubDivision  = postalAddress["CountrySubDivision"] as? Array<String> {
                branch.countrySubDivision = countrySubDivision.joined(separator: ", ").camelCasedString
            }
        }
        
        //workingHours
        if let availability = data["Availability"] as? NSDictionary,
            let standardAvailability = availability["StandardAvailability"] as? NSDictionary,
            let days = standardAvailability["Day"] as? NSArray {
            days.forEach ({
                if let workingHours = workingHoursWithJson($0 as! NSDictionary) {
                    branch.addToWorkingHours(workingHours)
                }
            })
        }
        return branch
    }
    
    private func workingHoursWithJson(_ day: NSDictionary) -> WorkingHours? {
        let workingHours = WorkingHours(context: CoreDataController.context)
        if let name = day["Name"] as? String {
            switch(name) {
            case "Monday" : workingHours.dayString = "1" + name
                break
            case "Tuesday" : workingHours.dayString = "2" + name
                break
            case "Wednesday" : workingHours.dayString = "3" + name
                break
            case "Thursday" : workingHours.dayString = "4" + name
                break
            case "Friday" : workingHours.dayString = "5" + name
                break
            case "Saturday" : workingHours.dayString = "6" + name
                break
            default : workingHours.dayString = ""
            }
            
            if let hours = (day["OpeningHours"] as? NSArray),
                let openingHours = hours.firstObject as? NSDictionary,
                    let openingTime = openingHours["OpeningTime"] as? String,
                        let closingTime = openingHours["ClosingTime"] as? String {
                workingHours.openingTime =  convertTo12Hour(from24Hour: openingTime)
                workingHours.closingTime = convertTo12Hour(from24Hour: closingTime)
            }
        }
        return workingHours
    }
}

//MARK:- Helpers
extension DataProvider {
    private func convertTo12Hour (from24Hour: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "H:mm"
        let date12Hour = dateFormatter.date(from: from24Hour)!
        dateFormatter.dateFormat = "h:mm a"
        return dateFormatter.string(from: date12Hour)
    }
}
