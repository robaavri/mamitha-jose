//
//  BranchDetailVC.swift
//  BranchListings
//
//  Created by Mamitha Jose on 3/11/19.
//  Copyright © 2019 Mamitha Jose. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class BranchDetailVC: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var tableView: UITableView!
    
    let regionRadius: CLLocationDistance = 1000
    let identifier = "marker"
    
    let reuseIdentifier = "branchDetailCellId"
    var branch:Branch? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //TODO spinner
        if let _ = branch {
            tableView.isHidden = false
            setUpMapView()
        }
    }
}

// MARK: - Table view data source and delegate
extension BranchDetailVC : UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0: if let _ = branch?.completeAddress { return 1 }
        case 1: if let _ = branch?.phoneNumber { return 1 }
        case 2: if let _ = branch?.faxNumber { return 1 }
        case 3: if let count = branch?.workingHours?.count { return (count+1) } // Adding Sunday
        default: return 0
        }
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)
        
        switch indexPath.section {
        case 0:
            if let address = branch?.completeAddress {
                cell.textLabel?.text = address
            }
        case 1:
            if let phoneNumber = branch?.phoneNumber {
                cell.textLabel?.text = phoneNumber
            }
        case 2: if let faxNumber = branch?.faxNumber {
            cell.textLabel?.text = faxNumber + " (Fax)"
            }
        default:
            if indexPath.row == 0 {
                cell.textLabel?.text = "Sunday Closed"
            }
            else {
                if let workingHours = branch?.workingHours?[indexPath.row - 1] as? WorkingHours,
                    let day = workingHours.day, let openingTime = workingHours.openingTime, let closingTime = workingHours.closingTime {
                    cell.textLabel?.text = day + " : " + openingTime + " - " + closingTime
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 3: return 20.0
        default : return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 3: return "Working Hours"
        default: return ""
        }
    }
}


// MARK: - Mapview delegate and helpers
extension BranchDetailVC: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        guard let annotation = annotation as? BranchAnnotation else { return nil }
        
        var view: MKMarkerAnnotationView
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            as? MKMarkerAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
        } else {
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x: -5, y: 5)
            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        return view
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView,
                 calloutAccessoryControlTapped control: UIControl) {
        let location = view.annotation as! BranchAnnotation
        let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
        location.mapItem().openInMaps(launchOptions: launchOptions)
    }
    
    private func setUpMapView() {
        if let location = branch!.location {
            updateRegionAndAddAnnotationWith(location)
        }
        else if let address = branch!.completeAddress {
            let geoCoder = CLGeocoder()
            geoCoder.geocodeAddressString(address) {[weak self] (placemarks, error) in
                if let placemarks = placemarks,
                    let location = placemarks.first?.location {
                    self?.updateRegionAndAddAnnotationWith(location)
                }
            }
        }
    }
    
    private func updateRegionAndAddAnnotationWith(_ location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center:location.coordinate,
                                                  latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
        let annotation = BranchAnnotation(title: branch!.name ?? "", coordinate:location.coordinate)
        mapView.addAnnotation(annotation)
    }
}
