//
//  BranchListingsVC.swift
//  BranchListings
//
//  Created by Mamitha Jose on 3/11/19.
//  Copyright © 2019 Mamitha Jose. All rights reserved.
//

import UIKit
import CoreLocation

class BranchListingsVC: UIViewController {
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    var branches = [Branch]()
    var filteredData = [Branch]()
    let reuseIdentifier = "branchCellId"
    var selectedBranch:Branch?
    var isSearching = false
    
    var locationManager : CLLocationManager?
    var currentLocation : CLLocation? {
        didSet {
            updateTableViewDisplay()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        findCurrentLocation()
    }
}

// MARK: - Data fetch helper
extension BranchListingsVC {
    private func fetchData() {
        if let branches = CoreDataController.getBranches(), !branches.isEmpty {
            self.branches = branches
            tableView.reloadData()
        }
        else {
//            CoreDataController.cleanDelete()
            DataProvider.shared.requestBranches() {[weak self] (error, success) in
                
                if let error = error {
                    print("Failed to fetch branches",error)
                    return
                }
                if let branches = CoreDataController.getBranches() {
                    self?.branches = branches
                    DispatchQueue.main.async {
                        self?.tableView.reloadData()
                    }
                }
            }
        }
    }
    
    //Navigation helper
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? BranchDetailVC {
            if let selected = selectedBranch {
                vc.branch = selected
                vc.title = selected.name
            }
        }
    }
}

// MARK: - location manager delegate and helpers
extension BranchListingsVC: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocation  = locations.last! as CLLocation
        manager.stopUpdatingLocation()
        locationManager = nil
    }
    
    private func findCurrentLocation() {
        currentLocation = nil
        if locationManager == nil {
            locationManager = CLLocationManager()
        }
        locationManager!.delegate = self
        locationManager!.desiredAccuracy = kCLLocationAccuracyBest
        locationManager!.requestAlwaysAuthorization()
        locationManager!.requestWhenInUseAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            locationManager!.startUpdatingLocation()
        }
    }
    
    private func branchesSortByDistance(branches:[Branch]) -> [Branch] {
        let sortedArray = branches.sorted {
            (currentLocation!.distance(from: ($0.location ?? CLLocation(latitude: 0, longitude: 0)))) <  (currentLocation!.distance(from: $1.location ?? CLLocation(latitude: 0, longitude: 0)))
        }
        return sortedArray
    }
}

// MARK: - SearchBar delegate
extension BranchListingsVC: UISearchBarDelegate {
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if searchBar.text == nil || searchBar.text == "" {
            isSearching = false
            view.endEditing(true)
            updateTableViewDisplay()
        }
        else {
            isSearching = true
            filteredData = CoreDataController.getBranchesInCity(searchBar.text ?? "" ) ?? []
            updateTableViewDisplay()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == nil || searchBar.text == "" {
            isSearching = false
            view.endEditing(true)
            updateTableViewDisplay()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

// MARK: - Table view data source and delegate and helpers
extension BranchListingsVC : UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching {
            return filteredData.count
        }
        return branches.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellId", for: indexPath) as! BranchListingTVCell
        
        if isSearching {
            let branch = filteredData[indexPath.row]
            cell.setUpCellWith(branch: branch, currentLocation: currentLocation)
        }
        else {
            let branch = branches[indexPath.row]
            cell.setUpCellWith(branch: branch, currentLocation: currentLocation)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedBranch = branches[indexPath.row]
        performSegue(withIdentifier: "branchDetailsSegue", sender: self)
    }
    
    private func updateTableViewDisplay() {
        if let _ = currentLocation {
            if isSearching {
                filteredData = branchesSortByDistance(branches: filteredData)
            }
            else {
                branches = branchesSortByDistance(branches: branches)
            }
        }
        tableView.reloadData()
    }
}

