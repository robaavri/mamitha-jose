//
//  BranchAnnotation.swift
//  BranchListings
//
//  Created by Mamitha Jose on 3/18/19.
//  Copyright © 2019 Mamitha Jose. All rights reserved.
//

import Foundation
import MapKit

class BranchAnnotation: NSObject, MKAnnotation {
    let title: String?
    let coordinate: CLLocationCoordinate2D
    
    init(title: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.coordinate = coordinate
        super.init()
    }
    
    func mapItem() -> MKMapItem {
        let placemark = MKPlacemark(coordinate: coordinate)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = title
        return mapItem
    }
}
