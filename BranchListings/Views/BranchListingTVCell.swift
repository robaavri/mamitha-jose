//
//  BranchListingTVCell.swift
//  BranchListings
//
//  Created by Mamitha Jose on 3/12/19.
//  Copyright © 2019 Mamitha Jose. All rights reserved.
//

import UIKit
import CoreLocation

class BranchListingTVCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
    func setUpCellWith(branch:Branch!, currentLocation:CLLocation?) {
        if let name = branch.name { nameLabel.text = name }
        if let address = branch.completeAddress { addressLabel.text = address }
        
        if let currentLocation = currentLocation,
            let location = branch.location {
            let distance = currentLocation.distance(from: location)
            let distanceInMeters = Measurement(value: distance, unit:UnitLength.meters)
            let miles = distanceInMeters.converted(to: .miles).value
            distanceLabel.text = "\(miles.roundTo(places: 2)) miles"
        }
        else {
            distanceLabel.text = ""
        }
    }
    
    override func prepareForReuse() {
        nameLabel.text = ""
        addressLabel.text = ""
        distanceLabel.text = ""
    }
}
