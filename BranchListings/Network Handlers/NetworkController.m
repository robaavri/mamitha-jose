//
//  NetworkController.m
//  BranchListings
//
//  Created by Mamitha Jose on 3/11/19.
//  Copyright © 2019 Mamitha Jose. All rights reserved.
//

#import "NetworkController.h"

@implementation NetworkController

typedef void (^NetworkDataRequestCompletion)(NSData* data, int statusCode, NSError *__nullable error);

+(void)requestDataWithMethod :(NSString*)method
                         url :(NSString*)urlString
       withcompletionHandler :(NetworkDataRequestCompletion) completionHandler {
    
    NSURL *url = [[NSURL alloc]initWithString:urlString];
    NSURLSession *session = [NSURLSession sharedSession];
    NSMutableURLRequest *request =[[NSMutableURLRequest alloc]initWithURL:url];
    request.HTTPMethod = method;
    
    NSURLSessionDataTask* task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse* _Nullable response, NSError * _Nullable error) {
        
        int statusCode = 0;
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        if ([httpResponse isKindOfClass:[NSHTTPURLResponse class]]) {
            statusCode = (int)httpResponse.statusCode;
        }
        completionHandler(data,statusCode, error);
    }];
    [task resume];
}
@end

