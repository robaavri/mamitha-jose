//
//  NetworkController.h
//  BranchListings
//
//  Created by Mamitha Jose on 3/11/19.
//  Copyright © 2019 Mamitha Jose. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkController : NSObject

+(void)requestDataWithMethod :(NSString*)method
                         url :(NSString*)urlString
       withcompletionHandler :(void (^)(NSData* data, int statusCode, NSError *__nullable error))completionHandler;

@end
