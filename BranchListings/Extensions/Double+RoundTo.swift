//
//  Double+RoundTo.swift
//  BranchListings
//
//  Created by Mamitha Jose on 3/12/19.
//  Copyright © 2019 Mamitha Jose. All rights reserved.
//

import Foundation

extension Double {
    func roundTo(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
